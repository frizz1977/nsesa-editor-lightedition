/**
 * Copyright 2013 European Parliament
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package org.nsesa.editor.gwt.core.client.ui.document.sourcefile.actionbar.resources;

import org.vectomatic.dom.svg.ui.SVGResource;
import org.vectomatic.dom.svg.ui.SVGResource.Validated;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * Resources for the
 * {@link org.nsesa.editor.gwt.core.client.ui.document.sourcefile.actionbar.ActionBarView}
 * . Date: 03/08/12 15:37
 * 
 * @author <a href="mailto:philip.luppens@gmail.com">Philip Luppens</a>
 * @version $Id$
 */
public interface Resources extends ClientBundle {

    @Source("comment_fill_16x14.png")
    ImageResource translate();

    @Source("lock_fill_12x16.png")
    ImageResource lock();

    @Source("move_16x16.png")
    ImageResource move();

    /*
     * @Source("amend.png") ImageResource amend();
     */

    // @Source("pen_alt2_16x16.png")

    // 30/08/2013 Sasso Mauro Add SVGResource,validated false to avoid XML
    // validation
    @Validated(validated = false)
    @Source("amend.svg")
    SVGResource amend();

    /*
     * @Source("new.png") ImageResource children();
     */

    // 30/08/2013 Sasso Mauro Add SVGResource,validated false to avoid XML
    // validation
    @Validated(validated = false)
    @Source("new.svg")
    SVGResource children();

    /*
     * 
     * 
     * @Source("cancel.png") ImageResource delete();
     */

    // 30/08/2013 Sasso Mauro Add SVGResource,validated false to avoid XML
    // validation
    @Validated(validated = false)
    @Source("cancel.svg")
    SVGResource delete();

    // 30 ago 2013 Sasso Mauro add svg button(img+text) by Andrea Panella for
    // test
    @Validated(validated = false)
    @Source("button_editor_new.svg")
    SVGResource button_editor_new();

}
