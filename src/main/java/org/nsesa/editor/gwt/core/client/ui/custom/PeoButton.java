/**
 * Copyright 2013 European Parliament
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package org.nsesa.editor.gwt.core.client.ui.custom;

import org.vectomatic.dom.svg.ui.SVGImage;
import org.vectomatic.dom.svg.ui.SVGResource;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.Button;

/**
 * Description: Class that create a custom button for the P.E.O. with label text
 * and img
 * 
 * @author Sasso Mauro
 * 
 * Date: 24/08/2013
 */

public class PeoButton extends Button {

    /**
     * variable used to store the text inside the button
     */
    private String text;

    // /////////////////////////////////////////////////////////
    //
    // Constructor
    //
    // /////////////////////////////////////////////////////////

    public PeoButton() {
        super();

    }

    // /////////////////////////////////////////////////////////
    //
    // Getters / Setters
    //
    // /////////////////////////////////////////////////////////

    /**
     * property used to store the image resource 30/08/2013 Update commented
     * Imageresource(if not used anymore,removed),add SVGResource
     */

    /*
     * public void setResource(ImageResource imageResource) { Image img = new
     * Image(imageResource); String definedStyles =
     * img.getElement().getAttribute("style");
     * img.getElement().setAttribute("style", definedStyles +
     * "; vertical-align:middle;"); DOM.insertBefore(getElement(),
     * img.getElement(), DOM.getFirstChild(getElement())); }
     */

    public void setResource(SVGResource imageResource) {
        SVGImage img = new SVGImage(imageResource);
        String definedStyles = img.getElement().getAttribute("style");
        img.getElement().setAttribute("style", definedStyles + "; vertical-align:middle;");
        DOM.insertBefore(getElement(), img.getElement(), DOM.getFirstChild(getElement()));
    }

    /**
     * property used to store the text
     */

    @Override
    public void setText(String text) {
        this.text = text;
        Element span = DOM.createElement("span");
        span.setInnerText(text);
        span.setAttribute("style", "margin-left:5px; vertical-align:middle;");
        DOM.insertChild(getElement(), span, 0);
    }

    /**
     * property used to get the text
     */

    @Override
    public String getText() {
        return this.text;
    }
}
