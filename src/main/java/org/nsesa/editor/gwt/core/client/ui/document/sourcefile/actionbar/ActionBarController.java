/**
 * Copyright 2013 European Parliament
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package org.nsesa.editor.gwt.core.client.ui.document.sourcefile.actionbar;

import static org.nsesa.editor.gwt.core.client.util.Scope.ScopeValue.DOCUMENT;

import java.util.logging.Logger;

import org.nsesa.editor.gwt.core.client.event.document.DocumentScrollEvent;
import org.nsesa.editor.gwt.core.client.event.document.DocumentScrollEventHandler;
import org.nsesa.editor.gwt.core.client.event.widget.OverlayWidgetDeleteEvent;
import org.nsesa.editor.gwt.core.client.event.widget.OverlayWidgetModifyEvent;
import org.nsesa.editor.gwt.core.client.event.widget.OverlayWidgetNewEvent;
import org.nsesa.editor.gwt.core.client.event.widget.OverlayWidgetNewEventHandler;
import org.nsesa.editor.gwt.core.client.ui.document.DocumentController;
import org.nsesa.editor.gwt.core.client.ui.document.DocumentEventBus;
import org.nsesa.editor.gwt.core.client.ui.document.sourcefile.SourceFileController;
import org.nsesa.editor.gwt.core.client.ui.document.sourcefile.actionbar.create.ActionBarCreatePanelController;
import org.nsesa.editor.gwt.core.client.ui.document.sourcefile.actionbar.create.ActionBarCreatePanelView;
import org.nsesa.editor.gwt.core.client.ui.overlay.document.OverlayWidget;
import org.nsesa.editor.gwt.core.client.util.Scope;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.TouchEndEvent;
import com.google.gwt.event.dom.client.TouchEndHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.*;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * The controller for the action bar component. This action bar is displayed
 * when hoovering an amendable {@link OverlayWidget} in the source text view
 * displayed in the {@link SourceFileController}.
 * <p/>
 * This toolbar gives access to commonly used actions such as modifying,
 * deleting, creating new elements, as well as displaying information about the
 * location of the {@link OverlayWidget} in the document.
 * <p/>
 * Date: 24/06/12 21:42
 *
 * @author <a href="mailto:philip.luppens@gmail.com">Philip Luppens</a>
 * @version $Id$
 */
@Singleton
@Scope(DOCUMENT)
public class ActionBarController {

    private static final Logger LOG = Logger.getLogger(ActionBarController.class.getName());

    /**
     * The main view for this component.
     */
    protected final ActionBarView view;

    /**
     * The CSS resource.
     */
    protected final ActionBarViewCss actionBarViewCss;

    /**
     * The document scoped event bus for communication between components in a
     * {@link org.nsesa.editor.gwt.core.client.ui.document.DocumentController}.
     */
    protected final DocumentEventBus documentEventBus;

    /**
     * The child controller to pop up a panel with the possibilities for new
     * element to be nested as children or as siblings.
     */
    protected final ActionBarCreatePanelController actionBarCreatePanelController;

    /**
     * The popup panel that will hold the new elements or siblings for this
     * {@link OverlayWidget}.
     */
    protected final PopupPanel popupPanel;

    /**
     * The overlay widget that is currently the target of this action bar
     * component.
     */
    protected OverlayWidget overlayWidget;

    /**
     * The parent sourcefile controller. Gives access to the
     * {@link org.nsesa.editor.gwt.core.client.ui.document.DocumentController}.
     */
    protected SourceFileController sourceFileController;
    private HandlerRegistration modifyHandlerRegistration;
    private HandlerRegistration deleteHandlerRegistration;
    private HandlerRegistration childHandlerRegistration;
    private com.google.web.bindery.event.shared.HandlerRegistration documentScrollEventHandlerRegistration;
    private com.google.web.bindery.event.shared.HandlerRegistration amendmentContainerCreateEventHandlerRegistration;
    private com.google.web.bindery.event.shared.HandlerRegistration overlayWidgetNewEventHandlerRegistration;

    @Inject
    public ActionBarController(final DocumentEventBus documentEventBus, final ActionBarView view, final ActionBarViewCss actionBarViewCss,
                               final ActionBarCreatePanelController actionBarCreatePanelController) {
        this.documentEventBus = documentEventBus;
        this.view = view;
        this.actionBarViewCss = actionBarViewCss;

        this.popupPanel = new DecoratedPopupPanel(true);
        this.actionBarCreatePanelController = actionBarCreatePanelController;
        this.actionBarCreatePanelController.setActionBarController(this);
        this.actionBarCreatePanelController.registerListeners();
    }

    /**
     * Sets the parent source file controller both on this component, and the
     * child {@link ActionBarCreatePanelController}.
     *
     * @param sourceFileController the parent source file controller
     */
    public void setSourceFileController(SourceFileController sourceFileController) {
        this.sourceFileController = sourceFileController;
        this.actionBarCreatePanelController.setSourceFileController(sourceFileController);
    }

    public void registerListeners() {
        // translate the touch on the modify handler into a request to create a
        // new modification amendment
        modifyHandlerRegistration = view.getModifyHandler().addTouchEndHandler(new TouchEndHandler() {
            @Override
            public void onTouchEnd(TouchEndEvent event) {
                onModifyTouchEnd(event);
            }
        });
        // translate the touch on the delete handler into a request to create a
        // new deletion amendment
        deleteHandlerRegistration = view.getDeleteHandler().addTouchEndHandler(new TouchEndHandler() {
            @Override
            public void onTouchEnd(TouchEndEvent event) {
                onDeleteTouchEnd(event);
            }
        });
        // translate the touch on the child handler into a request to create a
        // new element amendment
        childHandlerRegistration = view.getChildHandler().addTouchEndHandler(new TouchEndHandler() {
            @Override
            public void onTouchEnd(TouchEndEvent event) {
                onNewTouchEnd(event);
            }
        });

        /**
         * The following have been translated to TouchEnd events.
         * @author Vincenzo Abate
         * @date 11/02/2014
         */
        // translate the click on the modify handler into a request to create a
        // new modification amendment
//        modifyHandlerRegistration = view.getModifyHandler().addClickHandler(new ClickHandler() {
//            @Override
//            public void onClick(ClickEvent event) {
//                onModifyClick(event);
//            }
//        });
//        // translate the click on the delete handler into a request to create a
//        // new deletion amendment
//        deleteHandlerRegistration = view.getDeleteHandler().addClickHandler(new ClickHandler() {
//            @Override
//            public void onClick(ClickEvent event) {
//                onDeleteClick(event);
//            }
//        });
//        // translate the click on the child handler into a request to create a
//        // new element amendment
//        childHandlerRegistration = view.getChildHandler().addClickHandler(new ClickHandler() {
//            @Override
//            public void onClick(ClickEvent event) {
//                onNewClick(event);
//            }
//        });
        // we hide this toolbar as soon as the user scrolls the content
        documentScrollEventHandlerRegistration = documentEventBus.addHandler(DocumentScrollEvent.TYPE, new DocumentScrollEventHandler() {
            @Override
            public void onEvent(DocumentScrollEvent event) {
                if (event.getDocumentController() == sourceFileController.getDocumentController() || sourceFileController.getDocumentController() == null) {
                    onScroll();
                }
            }
        });
        overlayWidgetNewEventHandlerRegistration = documentEventBus.addHandler(OverlayWidgetNewEvent.TYPE, new OverlayWidgetNewEventHandler() {
            @Override
            public void onEvent(OverlayWidgetNewEvent event) {
                popupPanel.hide();
            }
        });
    }

    /**
     * The following have been translated to TouchEnd events.
     *
     * @author Vincenzo Abate
     * @date 11/02/2014
     */
    // translate the click on the modify handler into a request to create a
    // new modification amendment
//    protected void onModifyClick(ClickEvent event) {
//        event.stopPropagation();
//        if (overlayWidget != null) {
//            documentEventBus.fireEvent(new OverlayWidgetModifyEvent(overlayWidget));
//        }
//    }

    // translate the click on the delete handler into a request to create a
    // new deletion amendment
//    protected void onDeleteClick(ClickEvent event) {
//        event.stopPropagation();
//        if (overlayWidget != null) {
//            documentEventBus.fireEvent(new OverlayWidgetDeleteEvent(overlayWidget));
//        }
//    }

    // translate the click on the child handler into a request to create a
    // new element amendment
//    protected void onNewClick(ClickEvent event) {
//        event.stopPropagation();
//        if (overlayWidget != null) {
//            actionBarCreatePanelController.setOverlayWidget(overlayWidget);
//            final ActionBarCreatePanelView createPanelView = actionBarCreatePanelController.getView();
//            createPanelView.asWidget().setVisible(true);
//            popupPanel.setWidget(createPanelView);
//            popupPanel.showRelativeTo(view.getChildHandler());
//            popupPanel.show();
//        }
//    }

    // translate the touch on the modify handler into a request to create a
    // new modification amendment
    protected void onModifyTouchEnd(TouchEndEvent event) {
        event.stopPropagation();
        if (overlayWidget != null) {
            documentEventBus.fireEvent(new OverlayWidgetModifyEvent(overlayWidget));
        }
    }

    // translate the touch on the delete handler into a request to create a
    // new deletion amendment
    protected void onDeleteTouchEnd(TouchEndEvent event) {
        event.stopPropagation();
        if (overlayWidget != null) {
            documentEventBus.fireEvent(new OverlayWidgetDeleteEvent(overlayWidget));
        }
    }

    // translate the touch on the child handler into a request to create a
    // new element amendment
    protected void onNewTouchEnd(TouchEndEvent event) {
        event.stopPropagation();
        if (overlayWidget != null) {
            actionBarCreatePanelController.setOverlayWidget(overlayWidget);
            final ActionBarCreatePanelView createPanelView = actionBarCreatePanelController.getView();
            createPanelView.asWidget().setVisible(true);
            popupPanel.setWidget(createPanelView);
            popupPanel.showRelativeTo(view.getChildHandler());
            popupPanel.show();
        }
    }

    protected void onScroll() {
        // add the visibility to the toolbar on scroll 27/08/2013
        if (overlayWidget != null) {
            view.asWidget().setVisible(true);
            overlayWidget.asWidget().removeStyleName(actionBarViewCss.hover());
            /**
             * Bug fix: vertical toolbar has to be comoving with respect to the OverlayWidget witch is attached to.
             * @author Vincenzo Abate
             * @date 11/02/2014
             */
            adaptPosition(sourceFileController.getActiveOverlayWidget().asWidget());
        }
        // old instruction
        // view.asWidget().setVisible(false);

    }

    /**
     * Removes all registered event handlers from the event bus and UI.
     */
    public void removeListeners() {
        modifyHandlerRegistration.removeHandler();
        deleteHandlerRegistration.removeHandler();
        childHandlerRegistration.removeHandler();
        documentScrollEventHandlerRegistration.removeHandler();
        amendmentContainerCreateEventHandlerRegistration.removeHandler();
        overlayWidgetNewEventHandlerRegistration.removeHandler();
    }

    /**
     * Get the view for this component.
     *
     * @return the view
     */
    public ActionBarView getView() {
        return view;
    }

    /**
     * Set the current overlay widget - normally done via the
     * {@link #attach(org.nsesa.editor.gwt.core.client.ui.overlay.document.OverlayWidget, org.nsesa.editor.gwt.core.client.ui.document.DocumentController)}
     * method.
     *
     * @param overlayWidget the overlay widget
     */
    public void setOverlayWidget(final OverlayWidget overlayWidget) {
        this.overlayWidget = overlayWidget;
    }

    /**
     * Enable or disable the delete link to be visible.
     *
     * @param allowDelete <tt>true</tt> if you want the delete link to be visible.
     */
    public void setAllowDelete(final boolean allowDelete) {
        view.getDeleteHandler().setVisible(!allowDelete);
    }

    /**
     * Enable or disable the modify link to be visible.
     *
     * @param allowModify <tt>true</tt> if you want the modify link to be visible.
     */
    public void setAllowModify(final boolean allowModify) {
        view.getModifyHandler().setVisible(!allowModify);
    }

    /**
     * Enable or disable the move link to be visible.
     *
     * @param allowMove <tt>true</tt> if you want the move link to be visible.
     */
    public void setAllowMove(final boolean allowMove) {
        view.getMoveHandler().setVisible(!allowMove);
    }

    /**
     * Enable or disable the bundle link to be visible.
     *
     * @param allowBundle <tt>true</tt> if you want the bundle link to be visible.
     */
    public void setAllowBundle(final boolean allowBundle) {
        view.getBundleHandler().setVisible(!allowBundle);
    }

    /**
     * Enable or disable the new element link to be visible.
     *
     * @param allowChild <tt>true</tt> if you want the new element link to be visible.
     */
    public void setAllowChild(final boolean allowChild) {
        view.getChildHandler().setVisible(!allowChild);
    }

    /**
     * Enable or disable the translate link to be visible.
     *
     * @param allowTranslate <tt>true</tt> if you want the translate link to be visible.
     */
    public void setAllowTranslate(final boolean allowTranslate) {
        view.getTranslateHandler().setVisible(!allowTranslate);
    }

    /**
     * Set the location to be shown for the current {@link OverlayWidget}.
     *
     * @param location the location to be shown
     */
    public void setLocation(final String location) {
        view.setLocation(location);
    }

    /**
     * 'Detaches' the actionBar from the overlayWidget selected.
     *
     * @author Vincenzo Abate
     * @date 13/02/2014
     */
    public void detach() {
        if (view.asWidget().isVisible() && this.overlayWidget != null) {
            view.asWidget().setVisible(false);
            RootLayoutPanel.get().remove(view);
            this.overlayWidget = null;
        }
    }

    /**
     * 'Attaches' itself to the given <tt>target</tt> amendable overlay widget,
     * changing the CSS style of the overlay widget, and position the toolbar
     * right above it via a call to
     * {@link #adaptPosition(com.google.gwt.user.client.ui.Widget)}.
     *
     * @param target             the target overlay widget
     * @param documentController the containing document controller
     */
    public void attach(final OverlayWidget target, final DocumentController documentController) {
        // only perform the world if the new target overlay widget is actually
        // different
        // this made a huge difference in re-flow/repainting of the browser
        if (overlayWidget != target || !view.asWidget().isVisible()) {
            // if our action bar view has not yet been added to the rootpanel,
            // then do so now.
            if (!view.asWidget().isAttached()) {
                RootLayoutPanel.get().add(view);
            }
            // hide the popup panel with
            popupPanel.hide();

//           make sure it is visible
            view.asWidget().setVisible(true);

            /**
             * We don't use the event MouseHover.
             */
            // if we had a previous widget that was selected, make sure to
            // remove its special action css
            // done this way because onmouseout is not reliable enough
//            if (overlayWidget != null) {
//                overlayWidget.asWidget().removeStyleName(actionBarViewCss.hover());
//                view.asWidget().removeStyleName(actionBarViewCss.hover());
//            }

            this.overlayWidget = target;
//            if (overlayWidget.getType().equalsIgnoreCase("paragraph")) {
//                /*
//                 * TODO Insert a specific class for the paragraph hover to
//                 * resolve span problem(might be one solution,maybe
//                 * others!).Actually uses hover generic class
//                 */
//                overlayWidget.asWidget().addStyleName(actionBarViewCss.hover());
//
//            } else {
//                overlayWidget.asWidget().addStyleName(actionBarViewCss.hover());
//            }
//            view.asWidget().removeStyleName(actionBarViewCss.hover());

            adaptPosition(overlayWidget.asWidget());
        }
    }

    /**
     * Adapts the position of this 'hoovering' toolbar to be placed just above
     * the {@link OverlayWidget}.
     */
    /**
     * New adaptPosition is required to be used with the vertical toolbar.
     *
     * @param container
     * @author Vincenzo Abate
     * @date 11/02/2014
     */
    public void adaptPosition(final Widget container) {
        // hide the panel with our creation elements
        actionBarCreatePanelController.getView().asWidget().setVisible(false);
        if (overlayWidget != null) {

//            get the current style of the ActionBar.
            final Style style = view.asWidget().getElement().getStyle();

            /**
             * old instructions for the horizontal toolbar: useless.
             */
//          final int coordinateY = overlayWidget.asWidget().getAbsoluteTop() - (view.asWidget().getOffsetHeight() - 1);
//          we need to limit the width to make sure it does not
//            final int width = (container.getOffsetWidth() + container.getAbsoluteLeft()) - overlayWidget.asWidget().getAbsoluteLeft();
//            style.setWidth((width), Style.Unit.PX)
//          style.setLeft(overlayWidget.asWidget().getAbsoluteLeft(), Style.Unit.PX);

            /**
             * the left border of the vertical toolbar corresponds to the right border of the current OverlayWidget.
             * @author Vincenzo Abate
             * @date 11/02/2014
             */
//            style.setLeft(sourceFileController.getContentController().getView().getScrollPanel().getAbsoluteLeft() + sourceFileController.getContentController().getView().getScrollPanel().getOffsetWidth() - view.getActionPanel().asWidget().getOffsetWidth() - 1, Style.Unit.PX);
            style.setLeft(sourceFileController.getContentController().getView().getContentPanel().getAbsoluteLeft() + sourceFileController.getContentController().getView().getContentPanel().getOffsetWidth() + 1, Style.Unit.PX);
            adaptVisibility();
        }
    }

    /**
     * New adaptVisibility is required to be used with the vertical toolbar.
     *
     * @author Vincenzo Abate
     * @date 11/02/2014
     */
    public void adaptVisibility() {

//        final int widgetTop = overlayWidget.asWidget().getAbsoluteTop();
//        final int actionBarHeight = view.asWidget().getOffsetHeight();
//        GWT.log("actionBarHeight -----> " + actionBarHeight);
//        final int scrollPanelTop = sourceFileController.getContentController().getView().getScrollPanel().getAbsoluteTop();
//        // only show if we are fully visible
//        view.asWidget().setVisible(widgetTop - actionBarHeight >= scrollPanelTop);

        /**
         * New adaptVisibility implementation for the vertical toolbar.
         * @author Vincenzo Abate
         * @date 11/02/2014
         */
//        final int bottomSourceFileViewEdgeY = sourceFileController.getContentController().getView().asWidget().getAbsoluteTop() + sourceFileController.getContentController().getView().asWidget().getOffsetHeight();
        final int bottomOverlayWidgetEdgeY = overlayWidget.asWidget().getAbsoluteTop() + overlayWidget.asWidget().getOffsetHeight();
        final int bottomOverlayWidgetBottomSourceFileViewDistance = sourceFileController.getContentController().getView().getContentPanel().getAbsoluteTop() + sourceFileController.getContentController().getView().getContentPanel().getOffsetHeight() - bottomOverlayWidgetEdgeY;
        Style style = view.asWidget().getElement().getStyle();

//          if there is enough vertical space to the vertical toolbar being fully visible starting from the top of the OverlayWidget:
        if (bottomOverlayWidgetBottomSourceFileViewDistance >= view.getActionPanel().getOffsetHeight())
//              design the toolbar starting from the top of the OverlayWidget
            style.setTop(overlayWidget.asWidget().getAbsoluteTop(), Style.Unit.PX);
        else
//              design the toolbar starting from the bottom of SourceFileView
            style.setTop(sourceFileController.getContentController().getView().getContentPanel().getAbsoluteTop() + sourceFileController.getContentController().getView().getContentPanel().getOffsetHeight() - view.getActionPanel().getOffsetHeight(), Style.Unit.PX);

        view.setVisible(true);
    }

    /**
     * The overlay widget we are considered to be 'attached' to.
     *
     * @return the overlay widget
     */
    public OverlayWidget getOverlayWidget() {
        return overlayWidget;
    }
}
